#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Caculator
 
  @tag1
  Scenario: Add Two numbers
    Given I have a Caculator
    When I add 2 and 3
    Then The result should be 5
    

  @tag2
  Scenario: Substract two numbers
    Given I have a Caculator
    When  I substract 5 and 2
    Then The result should be 3

  
