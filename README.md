# CVS Health, DevSecOps Software Engineering BDD with Java Repository

## Behavior Driven Development

Behavior-Driven Development (BDD) is based on TDD, but TDD is focused on the internal processes of software and precision of code performance (unit tests), while BDD puts requirements and Business Value of software at the top of software priorities (acceptance tests). Meanwhile, acceptance tests are often modeled according to the User Stories and acceptance criteria. These tests are normally described in simple words so people from the outside of the IT industry (like shareholders, business analytics, QA engineers and Project Managers) understand them better.
## Here’s how our development cycle looks like:
    1. Write a script on Gherkin
    2. Run the script and realize that nothing works properly
        2.1 Identify situations when this script must work
        2.2 Start checking these situations and realize that nothing functions well
        2.3 Identify and implement minimal functionality necessary for all the examples to come through the test
    3. Execute everything one more time. In case of new errors, go back to point 2.1
    4. The script works! Start creating a new script covering the major part of requirements

## Introduction

This repo contains a set of Java files that show how to use unit and BDD tests in tandem to test application code.

All unit-test were written using [Junit](https://junit.org/junit5/docs/current/user-guide/) and [Mockito](https://www.vogella.com/tutorials/Mockito/article.html). BDD test were written using [cucumber-js](https://cucumber.io).

There are Five BDD _features_  and the  _step definition_ files in this project:
## 
    1. Authentication
        1.1 logging_on.feature                                  :: LoginSteps.Java   
    2. Earning Points                                                                                                             
        2.1 earning_extra_points_from_status.feature            :: EarningPointsFromFlights.Java
        2.2 earning_points_from_purchases.feature               :: EarningPointsFromFlights.Java
    3. Viewing Flights 
        3.1 displaying_flight_details.feature                   :: FlightDetailsSteps.Java
    4. To Check the Status
        4.1 calculating_status_based_on_points.feature          :: EarningStatus.Java 
## 
<!-- The first feature is pretty simple and it only contains one "hard coded" scenario. The second feature(`earning-points-from-flights-table`) makes use of an _"examples" table_, a feature unique to BDD which makes it easier for the business to provide tangible businnes logic samples to development teams. Development teams can use BDD scenarios and their sample data as part of the acceptance critirea for the feature.

Each table row in the _feature_ file is treated as a "scenario" and it's executed as such by the BDD test(s). If the table contains 2 rows, the BDD test(s) will be run twice, once for each row in the table. -->

To test a _feature_ the developer writes a _step definition_ file which is then run by _cucumber_ to test each scenario. The currespondent _step definition_ files are mentioned above.

##### Folder Structure
## 
    1. src/main/java                       :: This contains an application's Java source files
    2. src/test/Java                       :: This contains Unit Tests and Step definitions files in Java      
        2.1 com.bddinaction.cucumber       :: Runner Files.
        2.2 com.bddinaction.cucumber.steps :: Step definition files
        2.3 com.bddinaction.junit          :: Unit test files 
    3. src/test/resources
        3.1 com.bddinaction.cucumber       :: Feature files specific resources used in this application.
## 
<!-- bddinactio->cucumber -- FeaturesFiles  -->
<!-- Inside `src`, you'll find the `controller` and `model` folders containing business logic and app models respectively.

_unit-tests_ (\*.spec.js) files are stored in the same folder as the source code. It's also OK to move _unit-tests_ files to a different folder to keep source code "clean", for now we're keep tests and source file together. -->

#### Developer Notes

**Software Stack Used**

 
    1. Cucumber         :: It is a company and its eponymous test framework that uses Gherkin.
    2. Gherkin          :: It is the Given-When-Then spec language.
    3. Maven            :: Build automation tool.
    4. Code Review Tools
        4.1 SonarLint   :: It lives only in the IDE. Its purpose is to give instantaneous feedback as you type your code. For this, it concentrates on what code you are adding or updating.
        4.2 Jacoco      :: Code coverage library for Java
        4.3 SonarCube   :: It is a central server that processes full analyses (triggered by the various SonarQube Scanners). Its purpose is to give a 360° vision of the quality of your code base. For this, it analyzes all the source lines of your project on a regular basis.
    5. IDE              :: Eclipse, Intellij, STS.
        



<!-- Even though this is a small POC, the code written follows an MVC(without the "V") approach to organize and store business logic. The controller and model classes in this POC, could easily be used in a REST base web-service or any UI framework. The controller and model are meant to work in tandem to encapsulate all business logic and to access the model's data via the controller only. -->

<!-- **About BDD and TDD**
Using TDD best practices I was able to create and test the business logic encapsulated in the controller as well as define a model class to store data. All unit-tests that came out as part of following TDD exist idenpendent of any BDD test, but that's not true for the controller and model (the "app's" business logic), which is the desire outcome.

Using BDD I was able to "exercise" the business logic encapsulated in the controller to ensure the _acceptance criteria_ for the "user story" was wet.

I concluded that, TDD allows the developer to "flesh out" solid and tested business logic (in this case in the form of a controller). The same business logic(controller code) is then used to validate acceptance criteria for user stories by testing this business logic(controller) using BDD tests.

In my opinion, BDD tests are similar to "integration test" with the **big** added bonus that acceptance criteria can be written in plain English and can include examples which are used to exercise BDD tests. -->

**About BDD**
Is my opinion that BDD is a great tool to use when BAs, developers, QA work together in the creation and validation of user stories because:

1. Plain english language can be used to describe scenarios and expectations
2. Scenarios can include sample data. The data in sample scenarios is used by developers while working on business logic for the application. Additionally, the same sample data is used by the developers when writting BDD tests to testing business logic.

**Challenges For The Organization**
BDD requires a different way to work and additional code. Teams interested in adapting BDD will benefit from more clear and consice user stories and acceptance criteria but, that comes at the cost of more test code.

The challenge is to recognize that even thought more time is spent in tests, the benefit of stable cleanner releases outweights the extra time need to write more tests.

The other challenge is the change in development culture. Teams need to be willing and able to recognize the benefit of BDD testing.
