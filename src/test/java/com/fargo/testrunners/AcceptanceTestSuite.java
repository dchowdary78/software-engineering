package com.fargo.testrunners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features= {"src/test/resources/appFeatrures"},
				glue= {"com.fargo.stepdef"}, // where exactly your step definitions are available
				plugin= {"pretty"}
		)

public class AcceptanceTestSuite {

}
